# Parasoft dotTEST Integration for GitLab

This project provides example pipelines that demonstrate how to integrate Parasoft dotTEST with GitLab. The integration enables you to run code analysis, collect code coverage data and test results with Parasoft dotTEST and review results directly in GitLab.

Parasoft dotTEST is a testing tool that automates software quality practices for C# and VB.NET applications. It uses a comprehensive set of analysis techniques, including pattern-based static analysis, dataflow analysis, metrics, code coverage, and unit testing to help you verify code quality and ensure compliance with industry standards, such as CWE or OWASP.

- Request [a free trial](https://www.parasoft.com/products/parasoft-dottest/dottest-request-a-demo/) to receive access to Parasoft dotTEST's features and capabilities.
- See the [user guide](https://docs.parasoft.com/display/DOTTEST20221) for information about Parasoft dotTEST's capabilities and usage.

Please visit the [official Parasoft website](http://www.parasoft.com) for more information about Parasoft dotTEST and other Parasoft products.

- [Static Analysis](#static-analysis)
    - [Quick start](#quick-start-SA)
    - [Example Pipelines](#example-pipelines-SA)
    - [Reviewing Analysis Results](#reviewing-analysis-results)
- [Unit Tests](#unit-tests)
  - [Quick start](#quick-start-UT)
  - [Example Pipelines](#example-pipelines-UT)
  - [Reviewing Test Results](#reviewing-test-results)
- [Code Coverage](#code-coverage)
    - [Quick start](#quick-start-CC)
    - [Example Pipelines](#example-pipelines-CC)
    - [Reviewing Coverage Data](#reviewing-coverage-data)

## Static Analysis

### <a name="quick-start-SA"></a>  Quick start

To analyze your code with Parasoft dotTEST and review analysis results in GitLab, you need to customize your pipeline to include a job that will:
* run dotTEST.
* upload the analysis report in the SAST format.
* upload the dotTEST analysis reports in other formats (XML, HTML, etc.).


#### Prerequisites

* This extension requires Parasoft dotTEST 2021.2 (or newer) with a valid Parasoft license.
* We recommend that you execute the pipeline on a GitLab runner with the following components installed and configured on the runner:
   - Visual Studio or Build Tools for Visual Studio to build your project
   - Parasoft dotTEST 2021.2 (or newer)

### <a name="example-pipelines-SA"></a> Example Pipelines

The following example shows a simple pipeline. The example assumes that dotTEST is run on a GitLab runner and the path to the `dottestcli` executable is available on `PATH`.

See also the example [.gitlab-ci.yml](https://gitlab.com/parasoft/dottest-gitlab/-/blob/main/pipelines/.gitlab-ci.yml) file.

```yaml
# This is a basic pipeline to help you get started with dotTEST integration to analyze a GitLab project.

stages:
  - test-sa

# Runs code analysis with dotTEST.
StaticAnalysis:
  stage: test-sa
  script:
    # Configures advanced reporting options and SCM integration.
    - $SCM_SETTINGS="-property report.format=xml,html,sast-gitlab "
    - $SCM_SETTINGS+="-property report.scontrol=min "
    - $SCM_SETTINGS+="-property scontrol.rep.type=git "
    - $SCM_SETTINGS+="-property scontrol.rep.git.url=$CI_PROJECT_URL "
    - $SCM_SETTINGS+="-property scontrol.rep.git.workspace=$CI_PROJECT_DIR "
    - $SCM_SETTINGS+="-property scontrol.rep.git.branch=$CI_COMMIT_BRANCH"
    - $DT_ARGS=$SCM_SETTINGS.Split(" ")
	
    # Launches dotTEST.
    - echo "Running dotTEST..."
    - dottestcli -solution "%CI_PROJECT_DIR%/**/*.sln" -config "builtin://Recommended .NET Core Rules" -settings report.properties -report reports $DT_ARGS
  
  artifacts:
    # Uploads analysis results in the GitLab SAST format, so that they are displayed in GitLab.
    reports:
      sast: reports/report.sast
    # Uploads all report files (.xml, .html, .sast) as build artifacts.
    paths:
      - reports/*

```

### Limiting the Scope of Analysis

If you want to limit the scope of analysis to only see the violations from the current branch, set the `GIT_DEPTH` parameter to 0 before the script to enable an unshallow clone of the repository. For details, see [Shallow cloning](https://docs.gitlab.com/ee/ci/large_repositories/#shallow-cloning).

```yaml
stage: test
  variables:
    GIT_DEPTH: 0
  script:
  ...
```

### Reviewing Analysis Results
When the pipeline completes, you can review the violations reported by dotTEST as code vulnerabilities:
* in the *Security* tab of the GitLab pipeline.
* on GitLab's Vulnerability Report.

You can click each violation reported by dotTEST to review the details and navigate to the code that triggered the violation.

### Baselining Static Analysis Results in Merge Requests
In GitLab, when a merge request is created, static analysis results generated for the branch to be merged are compared with the results generated for the integration branch. As a result, only new violations are presented in the merge request view, allowing developers to focus on the relevant problems for their code changes. 

#### Defining a Merge Request Policy
You can define a merge request policy for your integration branch that will block merge requests due to new violations. To configure this:
1. In your GitLab project view, go to **Security & Compliance>Policies**, and select **New policy**.
1. Select the **Scan result policy** type.
1. In the **Policy details** section, define a rule for Static Application Security Testing (select “IF SAST…”). Configure additional options, if needed.


## Unit Tests

### <a name="quick-start-UT"></a> Quick start

To collect test executions results of your code with Parasoft dotTEST and review test results in GitLab, you need to customize your pipeline to include a job that will:
* run dotTEST.
* use Saxon to convert dotTEST unit tests report to xUnit format.
* upload the transformed xUnit report.

#### Prerequisites

* This extension requires Parasoft dotTEST 2020.2 (or newer) with a valid Parasoft license.
* We recommend that you execute the pipeline on a GitLab runner with the following components installed and configured on the runner:
  - Visual Studio or Build Tools for Visual Studio to build your project
  - Parasoft dotTEST 2020.2 (or newer)
* To support xUnit format, you need following files:
  - Saxon-HE: copy the folder [here](saxon) or download from [Saxonica](https://www.saxonica.com/download/java.xml).
  - [XSLT file](xsl/xunit.xsl) for transforming from Parasoft unit tests report to xUnit report.

### <a name="example-pipelines-UT"></a> Example Pipelines

The following example shows a simple pipeline. The example assumes that dotTEST is run on a GitLab runner and the path to the `dottestcli` executable is available on `PATH`.

See also the example [.gitlab-ci.yml](https://gitlab.com/parasoft/dottest-gitlab/-/blob/main/pipelines/.gitlab-ci.yml) file.

```yaml
# This is a basic pipeline to help you get started with dotTEST integration to collect test executions results of a dotNet project.

stages:
  - test-ut

# Runs unit tests with dotTEST.
UnitTests:
  stage: test-ut
  script:
    # Launches dotTEST.
    - echo "Running dotTEST..."
    - dottestcli -solution "%CI_PROJECT_DIR%/**/*.sln" -config "builtin://Run VSTest Tests" -report reports

    # Convert dotTEST unit tests report to xUnit format.
    # When running on Windows, be sure to replace backslashes:
    # - $CI_PROJECT_DIR = $CI_PROJECT_DIR.Replace("\", "/")
    - path/to/dottest/bin/dottest/Jre_x64/bin/java -jar 'saxon/saxon-he-12.2.jar' -xsl:"xsl/xunit.xsl" -s:"reports/report.xml" -o:"reports/report-xunit.xml" -t pipelineBuildWorkingDirectory=${CI_PROJECT_DIR}
    # Notes: To use Saxon for transformation, a Java executable is required. dotTEST is bundled with Java which can be used for this purpose.

  # Uploads unit tests data in the GitLab xUnit format, so that they are displayed in GitLab.
  artifacts:
    reports:
      junit: reports/report-xunit.xml
```

### Reviewing Test Results
When the pipeline completes, you can review the test results handled by dotTEST in the *Tests* tab of the GitLab pipeline.


## Code Coverage

### <a name="quick-start-CC"></a> Quick start

To collect code coverage data of your code with Parasoft dotTEST and review coverage data in GitLab, you need to customize your pipeline to include a job that will:
* run dotTEST.
* use Saxon to convert dotTEST coverage report to Cobertura format.
* upload the transformed Cobertura coverage report.

#### Prerequisites

* This extension requires Parasoft dotTEST 2020.2 (or newer) with a valid Parasoft license.
* We recommend that you execute the pipeline on a GitLab runner with the following components installed and configured on the runner:
  - Visual Studio or Build Tools for Visual Studio to build your project
  - Parasoft dotTEST 2020.2 (or newer)
* To support Cobertura format, you need following files:
    - Saxon-HE: copy the folder [here](saxon) or download from [Saxonica](https://www.saxonica.com/download/java.xml).
  - [XSLT file](xsl/cobertura.xsl) for transforming from Parasoft coverage report to Cobertura report.

### <a name="example-pipelines-CC"></a> Example Pipelines

The following example shows a simple pipeline. The example assumes that dotTEST is run on a GitLab runner and the path to the `dottestcli` executable is available on `PATH`.

See also the example [.gitlab-ci.yml](https://gitlab.com/parasoft/dottest-gitlab/-/blob/main/pipelines/.gitlab-ci.yml) file.

```yaml
# This is a basic pipeline to help you get started with dotTEST integration to collect coverage of a dotNet project.

stages:        
  - test-cov

# Runs coverage analysis with dotTEST.
CodeCoverage:
  stage: test-cov
  script:
    # Launches dotTEST.
    - echo "Running dotTEST..."
    - dottestcli -solution "%CI_PROJECT_DIR%/**/*.sln" -config "builtin://Run VSTest Tests with Coverage" -report reports

    # Convert dotTEST coverage report to Cobertura format.
    # When running on Windows, be sure to replace backslashes:
    # - $CI_PROJECT_DIR = $CI_PROJECT_DIR.Replace("\", "/")
    - path/to/dottest/bin/dottest/Jre_x64/bin/java -jar 'saxon/saxon-he-12.2.jar' -xsl:"xsl/cobertura.xsl" -s:"reports/coverage.xml" -o:"reports/cobertura.xml" -t pipelineBuildWorkingDirectory=${CI_PROJECT_DIR}
    # Notes: To use Saxon for transformation, a Java executable is required. dotTEST is bundled with Java which can be used for this purpose.

  # Uploads code coverage data in the GitLab Cobertura format, so that they are displayed in GitLab.
  artifacts:
    reports:
      coverage_report:
        # Coverage report format.
        coverage_format: cobertura
        # Uploads Cobertura report file to enable test coverage visualization in Gitlab merge request.
        path: reports/cobertura.xml
```

### Reviewing Coverage Data
After the pipeline triggered by a merge request completes, you can review the code coverage data collected by dotTEST:
* in the file diff view of the GitLab Merge requests.

Please visit the official GitLab website for more information about [Test coverage visualization](https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html).

---
## About
dotTEST integration for GitLab - Copyright (C) 2022 Parasoft Corporation
